using System.ComponentModel.DataAnnotations;
namespace PlanesBooking.Models;

public class Bookings
{
    public int Id { get; set; }

    [DataType(DataType.Date)]
    [Display(Name = "Booking Date")]
    public DateTime BookDate { get; set; }

    [Display(Name = "Airport Name")]
    public int AirportId { get; set; }

    [Display(Name = "Plane Name")]
    public int PlaneId { get; set; }

    [Display(Name = "Tutor")]
    public int TutorId { get; set; }

    public string? Description { get; set; }

    //public Dictionary<string,string>? MetaData { get; set; } = new Dictionary<string, string>();

}