using System.ComponentModel.DataAnnotations;
namespace PlanesBooking.Models;

public class Tutors : GenericModel
{
    public int Id { get; set; }

    public string Name { get{ return FirstName + " " + LastName; } set{ Name = value; } }

    [Required]
    [Display(Name = "First Name")]
    public string FirstName { get; set; } 

    [Required]
    [Display(Name = "Last Name")]
    public string LastName { get; set; }

    [Required]
    public string Nationality { get; set; }

    [Required]
    public string Gender { get; set; }

    [DataType(DataType.Date)]
    public DateTime Birthday { get; set; }    

    //public Dictionary<string,string>? MetaData { get; set; } = new Dictionary<string, string>();
}