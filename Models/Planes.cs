using System.ComponentModel.DataAnnotations;
namespace PlanesBooking.Models;

public class Planes : GenericModel
{
    public int Id { get; set; }

    [Required]
    [Display(Name = "Plane Name")]
    public string Name { get; set; }

    [Required]
    [Display(Name = "Airport")]
    public int AirportId { get; set; }

    //public Dictionary<string,string>? MetaData { get; set; } = new Dictionary<string, string>();

}