namespace PlanesBooking.Models;

public class SystemUsers
{
    public int Id { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public int UserLevel { get; set; }

    

    //public Dictionary<string,string>? MetaData { get; set; } = new Dictionary<string, string>();
}