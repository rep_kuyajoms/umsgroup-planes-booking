namespace PlanesBooking.Models;

public class Clients
{
    public int Id { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Nationality { get; set; }

    public string Gender { get; set; }

    public DateTime Birthday { get; set; }

    public int UserLevel { get; set; }

    //public Dictionary<string,string>? MetaData { get; set; } = new Dictionary<string, string>();
}