using System.ComponentModel.DataAnnotations;
namespace PlanesBooking.Models;

public class Airports : GenericModel
{
    public int Id { get; set; }

    [Required]
    [Display(Name = "Airport Name")]
    public string Name { get; set; }

    public string? Address { get; set; }

    public string? Manager {get; set;}

    //public Dictionary<string,string>? MetaData { get; set; } = new Dictionary<string, string>();

}