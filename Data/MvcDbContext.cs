#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PlanesBooking.Models;

    public class MvcDbContext : DbContext
    {
        public MvcDbContext (DbContextOptions<MvcDbContext> options)
            : base(options)
        {
        }

        public DbSet<PlanesBooking.Models.Airports> Airports { get; set; }
        public DbSet<PlanesBooking.Models.Bookings> Bookings { get; set; }
        public DbSet<PlanesBooking.Models.Planes> Planes { get; set; }
        public DbSet<PlanesBooking.Models.Tutors> Tutors { get; set; }

        public void PopulateSampleData()
        {

        }
    }
