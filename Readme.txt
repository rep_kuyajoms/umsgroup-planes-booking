What works:
CRUD (Create, Read, Update and Delete) of Airports, Bookings, Planes and Tutors

What doesn't work:
Update - when relating to a another data from another table, the Id is shown instead of a dropdown
Delete - Do not delete data that is used as a foreign key in another table, relational database has not been setup. (e.g delete a plane or airport that is used in a booking)